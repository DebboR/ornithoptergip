/*
 * OrnithopterGip.c
 *
 * Created: 5/09/2013 19:20:30
 *  Author: Robbe Derks
 */
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "init.h"
#include "ServoControl.h"
#include "ControlPanel/ControlPanel.h"

volatile char valS1, valS2;

int main(void)
{
	initAtmega();
	char *names[4] = {"    OS1     ", "    OS2     ", "    IS1     ", "    IS2     "};
	char *vals[4] = {&valS1, &valS2, &S1INVAL, &S2INVAL};
	initMenu(names, vals, 4);
	while(1)
	{	
		MPUInit();
		readCPButtons();
		valS1 %= 5;
		valS2 %= 5;
		_delay_ms(15);
				
		setS1(valS1*20);
		setS2(valS2*20);
	}
	return 0;
}

volatile char currLed = 0;

ISR(TIMER3_COMPA_vect){
	AVR_TIME++;
	if(AVR_TIME%200 == 0){
		dispMenu();
	} if(AVR_TIME%500 == 0){
		currLed++;
		currLed %= 4;
		switch (currLed)
		{
			case 0:
				setCPLEDS(1, 0, 0, 0);
				break;
			case 1:
				setCPLEDS(0, 1, 0, 0);
				break;
			case 2:
				setCPLEDS(0, 0, 1, 0);
				break;
			case 3:
				setCPLEDS(0, 0, 0, 1);
				break;
			default:
				break;
		}
	}
}