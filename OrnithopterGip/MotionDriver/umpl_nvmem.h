/*
 * umpl_nvram.h
 *
 * Created: 5/2/2011 2:27:56 PM
 *  Author: sgurumani
 */ 


#ifndef UMPL_NVMEM_H_
#define UMPL_NVMEM_H_


#ifdef __cplusplus
extern "C" {
#endif

#include "mltypes.h"
#include <stddef.h>

//! \name Data type for NVRAM memory addresses.

typedef uint16_t nvram_addr_t;

//! \name Atmel platform non-volatile memory spaces.


#   define NVRAM_BASE_ADDR      0
#   define NVRAM_SIZE           4096

void nvram_read (nvram_addr_t src, void * dst, size_t count);
void nvram_write (nvram_addr_t dst, const void * src, size_t count, bool erase);

#ifdef __cplusplus
}
#endif



#endif /* UMPL_NVMEM_H_ */