
/*
 $License:
    Copyright (C) 2010 InvenSense Corporation, All Rights Reserved.
 $
 */
/******************************************************************************
 *
 * $Id: packet.c 4/14/2011 10:17:57 AM  phickey $
 *
 *****************************************************************************/

/** 
 *  @defgroup DATA-CLIENT
 *  @brief  Data Client Implementation.
 *          Provides a mechanism to send and receive packetized data.
 *
 *  @{
 *      @file   packet.c
 *      @brief  Implements a mechanism to send and receive packetized data
 *              over the USB CDC port. Depends on the ASF usb_cdc service
 *              and the cpu cycle_counter driver.
 *				
**/

/* ------------------ */
/* - Include Files. - */
/* ------------------ */
#include "packet.h"

/* --------------- */
/* -  Functions. - */
/* --------------- */

/**
 *  @brief sendPacket sends a 14-byte packet in a specific format on the udi_cdc port.
 *		   It Checks for timeout on a per-byte basis. Out of the 14 bits, 10 bits are 
 *		   for payload and the rest are for control.
 *		   The format of each packet is as follows:
 *			<center>packet[0] = $</center>
 *			<center>packet[1] = DATA_TYPE</center>
 *			<center>packet[2] to packet[12] = DATA</center>
 *			<center>packet[13] = '\\r'</center>
 *			<center>packet[14] = '\\n'</center>
 *
 *
 *  @return 0 if successful; non-zero if timed out
 */
int sendPacket(unsigned char type, unsigned char * payload)
{
    
    return 0;
}
/**
 *  @brief Once a request for data has been made to the daemon, the getPacket is
 *		   used to receive packets. It implements an automatic time out to prevent
 *		   a "blocked" state.
 *
 *  @return 0 if successful; non-zero if timed out
 */
int getPacket(unsigned char *type, unsigned char * payload)
{
	return 0;
}
