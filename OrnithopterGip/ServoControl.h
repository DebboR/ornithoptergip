/*
 * ServoControl.h
 *
 * Created: 31/10/2013 20:14:58
 *  Author: Robbe D
 */ 


#ifndef SERVOCONTROL_H_
#define SERVOCONTROL_H_

#include <avr/interrupt.h>
#include "init.h"

#define SIN_LOW 63
#define SIN_HIGH 135

#define S1_LOW 17
#define S1_HIGH 69
#define S2_LOW 17
#define S2_HIGH 69
#define S3_LOW 17
#define S3_HIGH 69
#define S4_LOW 2
#define S4_HIGH 19

extern volatile char S1INVAL;
extern volatile char S2INVAL;
extern volatile char S3INVAL;
extern volatile char S4INVAL;

void initPwmTimers();
void initServoInterrupts();

//val is between 0 and 100. 50 => middle position
void setS1(char val);
void setS2(char val);
void setS3(char val);
void setS4(char val);



#endif /* SERVOCONTROL_H_ */