/*
 * ServoControl.c
 *
 * Created: 28/10/2013 22:04:53
 *  Author: Robbe D
 */ 

#include "ServoControl.h"

volatile char S1INVAL;
volatile char S2INVAL;
volatile char S3INVAL;
volatile char S4INVAL;


void initPwmTimers(){
	//Setting up timer prescaler
	
	//Setting up Phase Correct PWM for S1, S2, S3
	TCCR1A = (1 << COM1A1) | (1 << COM1B1) | (1 << COM1C1);
	TCCR1B = (1 << WGM13) | (1 << CS12);
	ICR1 = 625;	//Ensures 50Hz operation (with 16MHz XTAL and 1/256 prescaler)
	
	//Setting up Phase Correct PWM (around 30Hz) for S4
	TCCR2A = (1 << COM2A1) | (1 << WGM20);
	TCCR2B = (1 << CS22) | (1 << CS21) | (1 << CS20);	
		
}

void initServoInterrupts(){
	//Initializing pin-change interrupt on INT4, 5, 6, 7
	EICRB = (1 << ISC70) | (1 << ISC60) | (1 << ISC50) | (1 << ISC40);
	EIMSK |= (1 << INT7) | (1 << INT6) | (1 << INT5) | (1 << INT4);
	
	//Initializing timer 4 as a reference
	TCCR4A = 0;
	TCCR4B = (1 << CS42);
	TCCR4C = 0;
}

void setS1(char val){
	if(val > 100){
		val = 100;
	}
	if(val < 0){
		val = 0;
	}
	char realVal = (val*((float)((float)(S1_HIGH - S1_LOW)/(float)100))) + S1_LOW;
	OCR1A = realVal;
}

void setS2(char val){
	if(val > 100){
		val = 100;
	}
	if(val < 0){
		val = 0;
	}
	char realVal = (val*((float)((float)(S2_HIGH - S2_LOW)/(float)100))) + S2_LOW;
	OCR1B = realVal;
}

void setS3(char val){
	if(val > 100){
		val = 100;
	}
	if(val < 0){
		val = 0;
	}
	char realVal = (val*((float)((float)(S3_HIGH - S3_LOW)/(float)100))) + S3_LOW;
	OCR1C = realVal;
}

void setS4(char val){
	if(val > 100){
		val = 100;
	}
	if(val < 0){
		val = 0;
	}
	char realVal = (val*((float)((float)(S4_HIGH - S4_LOW)/(float)100))) + S4_LOW;
	OCR2A = realVal;
}

ISR(INT4_vect){
	if(PINE & (1 << S1IN)){
		TCNT4 = 0;
	}
	else{
		S1INVAL = ((float)(TCNT4-SIN_LOW)) * ((float)((float)100 / (float)(SIN_HIGH-SIN_LOW)));
		if(S1INVAL > 100){
			S1INVAL = 100;
		} else if (S1INVAL < 0)
		{
			S1INVAL = 0;
		}
	}
}

ISR(INT5_vect){
	if(PINE & (1 << S2IN)){
		TCNT4 = 0;
	}
	else{
		S2INVAL = ((float)(TCNT4-SIN_LOW)) * ((float)((float)100 / (float)(SIN_HIGH-SIN_LOW)));
		if(S2INVAL > 100){
			S2INVAL = 100;
		} else if (S2INVAL < 0)
		{
			S2INVAL = 0;
		}
	}
}

ISR(INT6_vect){
	if(PINE & (1 << S3IN)){
		TCNT4 = 0;
	}
	else{
		S3INVAL = ((float)(TCNT4-SIN_LOW)) * ((float)((float)100 / (float)(SIN_HIGH-SIN_LOW)));
		if(S3INVAL > 100){
			S3INVAL = 100;
		} else if (S3INVAL < 0)
		{
			S3INVAL = 0;
		}
	}
}

ISR(INT7_vect){
	if(PINE & (1 << S4IN)){
		TCNT4 = 0;
	}
	else{
		S4INVAL = ((float)(TCNT4-SIN_LOW)) * ((float)((float)100 / (float)(SIN_HIGH-SIN_LOW)));
		if(S4INVAL > 100){
			S4INVAL = 100;
		} else if (S4INVAL < 0)
		{
			S4INVAL = 0;
		}
	}
}