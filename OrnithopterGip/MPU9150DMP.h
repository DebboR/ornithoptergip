/*
 * MPU9150DMP.h
 *
 * Created: 5/05/2014 20:42:08
 *  Author: robbe_000
 */ 


#ifndef MPU9150DMP_H_
#define MPU9150DMP_H_

#include "I2CLib.h"

#define DMP_FEATURE_3AXIS_QUAT 0x01
#define DMP_FEATURE_6AXIS_QUAT 0x02
#define DMP_FEATURE_TAP 0x04
#define DMP_FEATURE_ANDROID_ORIENT 0x08
#define DMP_FEATURE_GYRO_CAL 0x10
#define DMP_FEATURE_SEND_RAW_ACCEL 0x20
#define DMP_FEATURE_SEND_RAW_GYRO 0x40
#define DMP_FEATURE_SEND_CAL_GYRO 0x80

void DMPWriteSection(unsigned int memAddr, unsigned char len, unsigned char *data);

void DMPLoadMotionDriver();

unsigned int DMPOrientationMatrixToScalar(const signed char *mtx);
unsigned int DMPRowToScalar(const signed char *row);
void DMPSetOrientation();
void DMPSetFeatures(char mask);
void DMPSetState(unsigned char enable);

#endif /* MPU9150DMP_H_ */
