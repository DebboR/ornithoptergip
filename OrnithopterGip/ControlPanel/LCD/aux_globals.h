//  HD44780 LCD 4-bit IO mode Driver
//  (C) 2009 - 2012 Radu Motisan , radu.motisan@gmail.com , www.pocketmagic.net
//  All rights reserved.
//
//  HD44780.c: Definitions for LCD command instructions
//  The constants define the various LCD controller instructions which can be passed to the
//  function lcd_command(), see HD44780 data sheet for a complete description.

#pragma once

#include <avr/io.h> 
#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>
//custom headers
#include "HD44780.h"

//delay functions
#define F_CPU 16000000UL 		//Your clock speed in Hz (16Mhz here)

//-----------------delays---------------------------------------------------------
#define LOOP_CYCLES 8 				//Number of cycles that the loop takes

#define delay_us(num) delay_int(num/(LOOP_CYCLES*(1/(F_CPU/1000000.0))))

void delay_int(unsigned long delay);
//--------------------------------------------------------------------------------


#define PORT_ON( port_letter, number )			port_letter |= (1<<number)
#define PORT_OFF( port_letter, number )			port_letter &= ~(1<<number)
#define PORT_ALL_ON( port_letter, number )		port_letter |= (number)
#define PORT_ALL_OFF( port_letter, number )		port_letter &= ~(number)
#define FLIP_PORT( port_letter, number )		port_letter ^= (1<<number)
#define PORT_IS_ON( port_letter, number )		( port_letter & (1<<number) )
#define PORT_IS_OFF( port_letter, number )		!( port_letter & (1<<number) )
