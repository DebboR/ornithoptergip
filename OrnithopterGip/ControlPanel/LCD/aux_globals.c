//  HD44780 LCD 4-bit IO mode Driver
//  (C) 2009 - 2012 Radu Motisan , radu.motisan@gmail.com , www.pocketmagic.net
//  All rights reserved.
//
//  HD44780.c: Definitions for LCD command instructions
//  The constants define the various LCD controller instructions which can be passed to the
//  function lcd_command(), see HD44780 data sheet for a complete description.

#include "aux_globals.h"


void delay_int(unsigned long delay)
{
	while(delay--) asm volatile("nop");
};