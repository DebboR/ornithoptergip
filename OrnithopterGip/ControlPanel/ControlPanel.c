/*
 * ControlPanel.c
 *
 * Created: 12/01/2014 15:36:47
 *  Author: robbe_000
 */ 

#include "ControlPanel.h"

void initControlPanel(){
	lcd_init();
	lcd_clrscr();
	lcd_home();
}

#pragma region CPMENU

char currentLayer = 0;
char currentItem = 0;

char menuNumItems = 0;
char *menuTitles[255];
char *menuValues[255];

char dispBuffer[2][30];

void initMenu(char *titles[], char *values[], char numItems){
	menuNumItems = numItems;
	for(char i=0; i<menuNumItems; i++){
		menuTitles[i] = titles[i];
		menuValues[i] = values[i];
	}
}

void dispMenu(){
	sprintf(dispBuffer[0], "< %s >", menuTitles[currentItem]);
	sprintf(dispBuffer[1], "%d", *(menuValues[currentItem]));
	lcd_clrscr();
	lcd_home();
	lcd_string(dispBuffer[0]);
	lcd_setline(1);
	lcd_string(dispBuffer[1]);
}

#pragma endregion CPMENU

#pragma region CPLEDS
void setCPL1(char on){
	if(on){
		PORT_ON(PORTA, CPL1);
	} else {
		PORT_OFF(PORTA, CPL1);
	}
}

void setCPL2(char on){
	if(on){
		PORT_ON(PORTA, CPL2);
	} else {
		PORT_OFF(PORTA, CPL2);
	}
}

void setCPL3(char on){
	if(on){
		PORT_ON(PORTC, CPL3);
	} else {
		PORT_OFF(PORTC, CPL3);
	}
}

void setCPL4(char on){
	if(on){
		PORT_ON(PORTC, CPL4);
	} else {
		PORT_OFF(PORTC, CPL4);
	}
}

void setCPLEDS(char l1, char l2, char l3, char l4){
	setCPL1(l1);
	setCPL2(l2);
	setCPL3(l3);
	setCPL4(l4);
}
#pragma endregion CPLEDS

#pragma region CPBUTTONS

char VCPBUP = 0;
char VCPBDOWN = 0;
char VCPBLEFT = 0;
char VCPBRIGHT = 0;
char VCPBOK = 0;
char VCPBBACK = 0;

void readCPButtons(){
	if(PORT_IS_ON(PINA, CPBUP)){
		if(!VCPBUP){			
			VCPBUP = 1;
			_delay_ms(50);
			doCPBUP();
		}		
	}
	else{
		VCPBUP = 0;
		_delay_ms(50);
	}
	
	if(PORT_IS_ON(PINA, CPBDOWN)){
		if(!VCPBDOWN){
			VCPBDOWN = 1;
			_delay_ms(50);
			doCPBDOWN();
		}
	}
	else{
		VCPBDOWN = 0;
		_delay_ms(50);
	}
	
	if(PORT_IS_ON(PINA, CPBLEFT)){
		if(!VCPBLEFT){
			VCPBLEFT = 1;
			_delay_ms(50);
			doCPBLEFT();
		}
	}
	else{
		VCPBLEFT = 0;
		_delay_ms(50);
	}
	
	if(PORT_IS_ON(PINA, CPBRIGHT)){
		if(!VCPBRIGHT){
			VCPBRIGHT = 1;
			_delay_ms(50);
			doCPBRIGHT();
		}
	}
	else{
		VCPBRIGHT = 0;
		_delay_ms(50);
	}
	
	if(PORT_IS_ON(PINA, CPBOK)){
		if(!VCPBOK){
			VCPBOK = 1;
			_delay_ms(50);
			doCPBOK();
		}
	}
	else{
		VCPBOK = 0;
		_delay_ms(50);
	}
	
	if(PORT_IS_ON(PINA, CPBBACK)){
		if(!VCPBBACK){
			VCPBBACK = 1;
			_delay_ms(50);
			doCPBBACK();
		}
	}
	else{
		VCPBBACK = 0;
		_delay_ms(50);
	}
}

void doCPBUP(){
	(*(menuValues[currentItem]))++;
}

void doCPBDOWN(){
	(*(menuValues[currentItem]))--;
}

void doCPBLEFT(){
	if(currentItem == 0){
		currentItem = menuNumItems - 1;
	} else {		
		currentItem--;
	}
}

void doCPBRIGHT(){
	currentItem++;
	if(currentItem >= menuNumItems){
		currentItem = 0;
	}
}

void doCPBOK(){
}

void doCPBBACK(){
}

#pragma endregion CPBUTTONS

