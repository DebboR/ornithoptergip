/*
 * ControlPanel.h
 *
 * Created: 12/01/2014 15:36:59
 *  Author: robbe_000
 */ 


#ifndef CONTROLPANEL_H_
#define CONTROLPANEL_H_

#include "ControlPanel/LCD/HD44780.h"
#include "ControlPanel/LCD/aux_globals.h"
#include "init.h"

void initControlPanel();

void setCPL1(char on);
void setCPL2(char on);
void setCPL3(char on);
void setCPL4(char on);
void setCPLEDS(char l1, char l2, char l3, char l4);

void readCPButtons();
void doCPBUP();
void doCPBDOWN();
void doCPBLEFT();
void doCPBRIGHT();
void doCPBOK();
void doCPBBACK();

void initMenu(char *titles[], char *values[], char numItems);
void dispMenu();

#endif /* CONTROLPANEL_H_ */