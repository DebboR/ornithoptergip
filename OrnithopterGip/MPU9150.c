/*
 * MPU9150.c
 *
 * Created: 4/05/2014 16:17:36
 *  Author: robbe_000
 */ 

#include "MPU9150.h"

void MPUInit(){
	//Power on (+ use stable X-gyro clock)
	I2CSetRegister(MPUADDR, 0x6B, 0x01);
	I2CSetRegister(MPUADDR, 0x6C, 0x00);
	
	//Enable DMP
	DMPLoadMotionDriver();
	MPUSetI2CBypass(0);
	MPUSetFIFO(1);
	DMPSetOrientation();
	DMPSetFeatures(DMP_FEATURE_6AXIS_QUAT | DMP_FEATURE_SEND_CAL_GYRO | DMP_FEATURE_SEND_RAW_ACCEL);
	MPUSetFIFORate(200);
	DMPSetState(1);
}

void MPUSetI2CBypass(unsigned char enabled){
	unsigned char controlReg;
	I2CReadRegister(MPUADDR, 0x6A, &controlReg);
	if(enabled){
		controlReg &= ~(1 << 5);
	} else {		
		controlReg |= (1 << 5);
	}
	I2CSetRegister(MPUADDR, 0x6A, controlReg);
}

void MPUSetFIFO(unsigned char enabled){
	unsigned char controlReg;
	I2CReadRegister(MPUADDR, 0x6A, &controlReg);
	if(enabled){		
		controlReg |= (1 << 6);
	} else {
		controlReg &= ~(1 << 6);
	}
	I2CSetRegister(MPUADDR, 0x6A, controlReg);
}

void MPUResetFIFO(){
	//Simpeler als documentatie, niet zeker of het werkt... (PPOF)
	unsigned char controlReg;
	I2CReadRegister(MPUADDR, 0x6A, &controlReg);
	controlReg |= (1 << 2);
	I2CSetRegister(MPUADDR, 0x6A, controlReg);
}

void MPUSetFIFORate(int freq){
	unsigned char regsEnd[12] = {0xFE, 0xF2, 0xAB, 0xC4, 0xAA, 0xF1, 0xDF, 0xDF, 0xBB, 0xAF, 0xDF, 0xDF};
	unsigned int div;
	unsigned char buffer[8];

	if (freq <= 200){
		div = (200 / freq) - 1;
		buffer[0] = (unsigned char)((div >> 8) & 0xFF);
		buffer[1] = (unsigned char)(div & 0xFF);
		DMPWriteSection(534, 2, buffer);
		DMPWriteSection(2753, 12, regsEnd);
	}
}

char MPUReadTemp(){
	unsigned int t;
	unsigned char temp;
	I2CReadRegister(MPUADDR, 0x42, &temp);
	t = temp;
	I2CReadRegister(MPUADDR, 0x41, &temp);
	t |= (temp << 8);
	int raw =  (int) t;

	return (char)(((float)raw/340.0) + 35.0);	
}

void MPUSetInterrupt(char enable, char usingDMP){
	unsigned char buffer;
	if(enable){
		if(usingDMP){
			buffer = 0x02;
		} else{
			buffer = 0x01;
		}
	} else{
		buffer = 0x00; 
	}
	I2CSetRegister(MPUADDR, 0x38, buffer);
}



