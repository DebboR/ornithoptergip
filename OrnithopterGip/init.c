/*
 * init.c
 *
 * Created: 3/10/2013 21:37:24
 *  Author: Robbe Derks
 */ 
#include "init.h"

long int AVR_TIME = 0;

void initAtmega(){
	//Enable interrupts
	sei();
	
	//Setting up the ports
	DDRA = (1<<CPL1) | (1<<CPL2);
	DDRB = (1<<S1OUT) | (1<<S2OUT) | (1<<S3OUT) | (1<<S4OUT);
	DDRC = (1<<CPL3) | (1<<CPL4);
	DDRD = (1<<PD0) | (1<<PD1);
	DDRE = 0;
	DDRF = 0;
	DDRG = 0;
	
	//Setting up timer3A as a time reference. (calling the interrupt each ms (250 ticks))
	TCCR3A = 0;
	TCCR3B = (1 << CS31) | (1 << CS30) | (1 << WGM32);
	OCR3A = 250;
	TIMSK3 = (1 << OCIE3A);
		
	//Initialize different software and hardware components
	initPwmTimers();
	initServoInterrupts();
	initControlPanel();
	I2CInit();
	MPUInit();
}