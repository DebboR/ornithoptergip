/*
 * MPU9150.h
 *
 * Created: 4/05/2014 16:17:54
 *  Author: robbe_000
 */ 


#ifndef MPU9150_H_
#define MPU9150_H_

#define MPUADDR 0xD0

#include "I2CLib.h"
#include "init.h"

#include "MPU9150DMP.h"

void MPUInit();
char MPUReadTemp();
void MPUSetI2CBypass(unsigned char enabled);
void MPUSetFIFO(unsigned char enabled);
void MPUResetFIFO();
void MPUSetFIFORate(int freq);
void MPUSetInterrupt(char enable, char usingDMP);

#endif /* MPU9150_H_ */