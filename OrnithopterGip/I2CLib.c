/*
 * I2CLib.c
 *
 * Created: 26/01/2014 21:50:38
 *  Author: robbe_000
 */ 

#include "I2CLib.h"

void I2CInit(){
	
	//Enable TWI-module
	PRR0 &= ~(1 << PRTWI);
	TWCR = (1 << TWEN) | (1 << TWEA);
		
	//Set pins to output
	DDRD |= (1 << PD1) | (1 << PD0);
		
	//Set frequency
	TWBR = 15;
	TWSR = 0;
}

void I2CSendStart(){
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
	while(!(TWCR & (1<<TWINT)));
	while((TWSR & 0xF8)!= 0x08);
}

void I2CSendRestart(){
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
	while(!(TWCR & (1<<TWINT)));
	while((TWSR & 0xF8)!= 0x10);
}

void I2CSendStop(){
	TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
	int maxError = 0;
	while(!(TWCR & (1<<TWSTO)) && maxError < 2000){
		maxError++;
	}
	_delay_us(25);
}

void I2CSendWriteAddress(unsigned char address){
	TWDR = address;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1<<TWINT)));
	while((TWSR & 0xF8)!= 0x18);
}

void I2CSendReadAddress(unsigned char address){
	TWDR = address;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1<<TWINT)));
	while((TWSR & 0xF8)!= 0x40);
}

void I2CSendData(unsigned char data){
	TWDR = data;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1<<TWINT)));
	while((TWSR & 0xF8)!= 0x28);
}

void I2CReadData(unsigned char *data){
	TWCR=(1<<TWINT)|(1<<TWEN);
	while(!(TWCR & (1<<TWINT)));
	while((TWSR & 0xF8) != 0x58);
	*data = TWDR;
}

void I2CSetRegister(unsigned char address, unsigned char reg, unsigned char data){
	I2CSendStart();
	I2CSendWriteAddress(address);
	I2CSendData(reg);
	I2CSendData(data);
	I2CSendStop();
}

void I2CReadRegister(unsigned char address, unsigned char reg, unsigned char *data){
	I2CSendStart();
	I2CSendWriteAddress(address);
	I2CSendData(reg);
	I2CSendRestart();
	I2CSendReadAddress(address + 1);
	I2CReadData(data);
	I2CSendStop();
}

void I2CSendMassData(unsigned char address, unsigned char reg, unsigned char *data, unsigned char dataLen){
	I2CSendStart();
	I2CSendWriteAddress(address);
	I2CSendData(reg);
	for(unsigned char i=0; i<dataLen; i++){
		I2CSendData(data[i]);
	}
	I2CSendStop();
}


