/*
 * I2CLib.h
 *
 * Created: 26/01/2014 21:51:00
 *  Author: robbe_000
 */ 


#ifndef I2CLIB_H_
#define I2CLIB_H_

#include <avr/io.h>
#include "init.h"

void I2CInit();
void I2CSendStart();
void I2CSendRestart();
void I2CSendStop();
void I2CSendWriteAddress(unsigned char address);
void I2CSendReadAddress(unsigned char address);
void I2CSendData(unsigned char data);
void I2CReadData(unsigned char *data);
void I2CSetRegister(unsigned char address, unsigned char reg, unsigned char data);
void I2CReadRegister(unsigned char address, unsigned char reg, unsigned char *data);
void I2CSendMassData(unsigned char address, unsigned char reg, unsigned char *data, unsigned char dataLen);

#endif /* I2CLIB_H_ */
