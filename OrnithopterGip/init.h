/*
 * init.h
 *
 * Created: 12/10/2013 22:47:35
 *  Author: Robbe D
 */ 


#ifndef INIT_H_
#define INIT_H_


#define F_CPU 16000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "ControlPanel/ControlPanel.h"
#include "ServoControl.h"
#include "I2CLib.h"
#include "MPU9150.h"

extern long int AVR_TIME;

//Servo input pins
#define S1IN PE4
#define S2IN PE5
#define S3IN PE6
#define S4IN PE7

//Servo output pins
#define S1OUT PB5
#define S2OUT PB6
#define S3OUT PB7
#define S4OUT PB4

//ControlPanel LED-pins
#define CPL1 PA7
#define CPL2 PA6
#define CPL3 PC0
#define CPL4 PC1

//ControlPanel Button-pins
#define CPBUP PA2
#define CPBDOWN PA3
#define CPBLEFT PA0
#define CPBRIGHT PA1
#define CPBOK PA4
#define CPBBACK PA5

//External pins
#define EX1 PF7
#define EX2 PF6
#define EX3 PF5
#define EX4 PF4
#define EX5 PF3
#define EX6 PF2
#define EX7 PF1
#define EX8 PF0

void initAtmega();

#endif /* INIT_H_ */